import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.Button;
import lejos.nxt.TouchSensor;
import lejos.nxt.Sound;

public class TouchSample {
	public static void main(String[] args) throws Exception {
		TouchSensor touch = new TouchSensor(SensorPort.S1);
		while (!Button.ESCAPE.isPressed()) {
			if (touch.isPressed()){
				LCD.clear();
				LCD.drawString("Pressed", 3, 4);
				Sound.playTone(2000, 1000);
			}
			if (!touch.isPressed()){
				LCD.clear();
				LCD.drawString("Released", 3, 4);
			}
		}
		LCD.drawString("Finished", 3, 4);
	}
}
