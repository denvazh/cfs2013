import lejos.nxt.*;
import lejos.robotics.navigation.*;

public class DifferentialTest {
	
	public static void main(String [] args) {
		//double diam = DifferentialPilot.WHEEL_SIZE_NXT1;
		double diam = DifferentialPilot.WHEEL_SIZE_NXT2;
		// double trackwidth = 16.4;
		double trackwidth = 15.5;
		
		DifferentialPilot robot = new DifferentialPilot(diam, trackwidth, Motor.B, Motor.C, false);
		robot.travel(100);
		Button.ENTER.waitForPressAndRelease();
		robot.rotate(1080);
	}
}
