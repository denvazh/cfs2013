import lejos.nxt.*;
import lejos.nxt.addon.*;
import lejos.robotics.Gyroscope;
import lejos.robotics.navigation.*;

/**
 * CABLE CONNECTIONS:
 * While viewing the picture of Orbot on page 366 of Intelligence Unleashed, 
 * plug the cables in as follows (e.g. the left motor is on the left side of the page):
 * Port B to the left motor (short cable)
 * Port C to the right motor (medium)
 * Port 2 to the right sensor (medium)
 * Port 4 to the left sensor  (medium)
 * 
 * Ensure all cables are wrapped around the frame of the robot to prevent cables
 * from wagging while the robot is in motion. 
 * 
 */
public class Orbot {
	public static void main(String [] args) throws Exception {
		
		float tireCompression = 0.3F;
		
		/* Code for dIMU Gyro Sensor by Dexter Industries
		SensorPort.S3.i2cEnable(I2CPort.HIGH_SPEED);
		DIMUGyro dimu = new DIMUGyro(SensorPort.S3);
		Gyroscope gx = dimu.getAxis(DIMUGyro.Axis.Z);
		Gyroscope gy = dimu.getAxis(DIMUGyro.Axis.X);
		*/
		
		/* Code for single Cruizecore gyro (need another gyro of any make for gx): 
		SensorPort.S1.i2cEnable(I2CPort.HIGH_SPEED);
		Gyroscope gy = new CruizcoreGyro(SensorPort.S1);
		*/
		
		// Code for HiTechnic Sensors:
		NXTMotor xMotor = new NXTMotor(MotorPort.B);
		GyroSensor gx = new GyroSensor(SensorPort.S2);
		
		NXTMotor yMotor = new NXTMotor(MotorPort.C);
		GyroSensor gy = new GyroSensor(SensorPort.S4);
		// END code for HiTechnic sensors		
		
		Ballbot bb = new Ballbot(xMotor, gx, yMotor, gy, MoveController.WHEEL_SIZE_NXT2-tireCompression);
		
		Button.ESCAPE.waitForPressAndRelease();
		System.exit(0);
	}
}