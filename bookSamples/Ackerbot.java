import lejos.nxt.*;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.*;

public class Ackerbot {

   static RegulatedMotor motor;
   static int left = 0;
   static int right = 0;
   static int center = 0;
   
   public static void main(String[] args) throws Exception {
      Ackerbot.calibrate(MotorPort.C);
      Ackerbot.recenter(Motor.C);
      Motor.C.setAcceleration(200);
      
      double MINTURN_RADIUS = 31.75;
      SteeringPilot p = new SteeringPilot(SteeringPilot.WHEEL_SIZE_NXT2, Motor.B,
         Motor.C, MINTURN_RADIUS, 48, -42);
      
      Navigator nav = new Navigator(p);
      nav.goTo(40, 50, 90);
      nav.goTo(0, 0, 0);
      
      p.arc(MINTURN_RADIUS, 360);  
      Button.ENTER.waitForPressAndRelease();
      p.arc(-MINTURN_RADIUS, 360);
   }

   public static void calibrate(MotorPort port) {
      NXTMotor m = new NXTMotor(port);
      m.setPower(20);
      m.backward();
      int old = -999999;
      while(m.getTachoCount() != old) {
         old = m.getTachoCount();
         try {
            Thread.sleep(500);
         } catch (InterruptedException e) {
         }
      }
      
      right = m.getTachoCount();
      center = (59 + right);
      left = center + 59;
   }
   
   public static void recenter(RegulatedMotor steering) {
      motor = steering;
      motor.setSpeed(100);
      motor.rotateTo(center);
      motor.flt();
      motor.resetTachoCount();
   }
}