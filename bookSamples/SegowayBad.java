import lejos.nxt.*;
import lejos.nxt.addon.GyroSensor;

public class SegowayBad {
	public static int speed = 0;
	
	public static void main (String[] args) throws Exception {
		GyroSensor s = new GyroSensor(SensorPort.S1);
		Motor.B.flt();
		Thread.sleep(1000); // give robot time to remain still
		s.recalibrateOffset();
		
		for(int i=0;i<5;i++) {
			Sound.beep();
			Thread.sleep(1000);
		}

		while (!Button.ESCAPE.isDown()) {
			int aVelocity = (int)s.getAngularVelocity();

			speed = Math.abs(aVelocity) * 30;

			Motor.B.setSpeed(speed);
			Motor.C.setSpeed(speed);


			boolean forward = aVelocity > 0;
			if (forward) {
				Motor.B.forward();
				Motor.C.forward();
			} else {
				Motor.B.backward();
				Motor.C.backward();
			}
			Thread.sleep(8); // 8 ms
		}

		Motor.B.flt();
		Motor.C.flt();
	}
}
