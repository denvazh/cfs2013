import lejos.nxt.*;
import lejos.nxt.addon.GyroSensor;
import lejos.robotics.*;
import lejos.robotics.navigation.*;

public class SegBumper {

	public static void main(String[] args) throws Exception {
		EncoderMotor left = new NXTMotor(MotorPort.B);
		EncoderMotor right = new NXTMotor(MotorPort.C);
		
		GyroSensor g = new GyroSensor(SensorPort.S1);
		
		UltrasonicSensor us = new UltrasonicSensor(SensorPort.S4);
		
		Segoway robot = new Segoway(left, right, g, SegowayPilot.WHEEL_SIZE_NXT1);
		Thread.sleep(3000);
		robot.wheelDriver(-120, -120); // Move forward
		while(!Button.ESCAPE.isDown()) {
			if(us.getDistance() < 40) {
				robot.wheelDriver(70, 35); // Arc back
				Thread.sleep(3000);
				robot.wheelDriver(-120, -120); // Move forward
			}
			Thread.sleep(350);
		}
	}
}