import lejos.pc.comm.NXTConnector;
import java.awt.event.*;
import java.awt.*;
import java.io.*;

public class SegowayClient extends Frame implements KeyListener{
	
	private static final long serialVersionUID = -5094659126966976519L;
	
	// Protocol values:
	public static int ROT_LEFT = 1;
	public static int ROT_RIGHT = 2;
	public static int FWD = 3;
	public static int BWD = 4;
	public static int STOP = 5;
	public static int PROG_QUIT = 99;

	// Keyboard control values:
	final int FORWARD = 87, // W = forward
	BACKWARD = 83, // S = backward
	LEFT = 65, // A = left turn
	RIGHT = 68, // D = right
	QUIT = 81; // Q = quit

	boolean steerreleased = true;
	boolean drivereleased = true;

	NXTConnector conn;
	DataOutputStream dos;

	public SegowayClient(String title) {
		super(title);

		conn = new NXTConnector();
		boolean connected = conn.connectTo("btspp://");

		if (!connected) {
			System.err.println("Failed to connect to any NXT");
			System.exit(1);
		}

		dos = new DataOutputStream(conn.getOutputStream());

		this.setBounds(0, 0, 300, 50);
		this.addKeyListener(this);
		this.setVisible(true);
	}

	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
		case FORWARD:
			if(drivereleased==true) sendCommand(FWD);
			steerreleased = false;
			break;
		case BACKWARD:
			if(drivereleased==true) sendCommand(BWD);
			steerreleased = false;
			break;
		case LEFT:
			if(steerreleased==true)  sendCommand(ROT_LEFT);
			steerreleased = false;
			break;
		case RIGHT:
			if(steerreleased==true) sendCommand(ROT_RIGHT);
			steerreleased = false;
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
		switch(e.getKeyCode()) {
		case FORWARD:
		case BACKWARD:
		case LEFT:
		case RIGHT:
			sendCommand(STOP);
			steerreleased = true;
			break;
		case QUIT:
			sendCommand(PROG_QUIT);
			System.exit(0);                
		}
	}

	public void keyTyped(KeyEvent e) {}

	private void sendCommand(int cmd) {
		try {
			dos.writeInt(cmd);
			dos.flush();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new SegowayClient("Enter commands");
	}
}

