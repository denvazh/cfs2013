import lejos.nxt.*;

public class TwirlerAware implements ButtonListener {
	
	public static void main(String [] args) throws Exception {
		TwirlerAware listener = new TwirlerAware();
		Button.ESCAPE.addButtonListener(listener);
		
		UltrasonicSensor us = new UltrasonicSensor(SensorPort.S1);
				
		Motor.B.setAcceleration(500);
		Motor.C.setAcceleration(500);
		Motor.B.forward();
		while(true) {
			if(us.getRange() < 40) {
				Sound.beep();
				Motor.B.setSpeed(800);
				Motor.C.setSpeed(800);
				Motor.B.backward();
				Motor.C.backward();
				Thread.sleep(4000);
				Motor.B.forward();
			}
				
			float speed1 = (float) (Math.random() * 800);
			float speed2 = (float) (Math.random() * 800);
			Motor.B.setSpeed(speed1);
			Motor.C.setSpeed(speed2);
			if(Math.random() < 0.5) {
				Motor.C.forward();
			} else {
				Motor.C.backward();
			}
			Thread.sleep(5000);
		}
	}

	public void buttonPressed(Button b) {}
	
	public void buttonReleased(Button b) {
		System.exit(0);
	}
}
