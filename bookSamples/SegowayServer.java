import java.io.*;
import lejos.nxt.*;
import lejos.nxt.addon.GyroSensor;
import lejos.nxt.comm.*;
import lejos.robotics.navigation.*;

public class SegowayServer implements ButtonListener {

  public static final int LEFT = 1;
  public static final int RIGHT = 2;
  public static final int FWD = 3;
  public static final int BWD = 4;
  public static final int STOP = 5;
  public static final int QUIT = 99;
  
  static ArcRotateMoveController robot;
  
  public static void main(String[] args) {
    
    Button.ESCAPE.addButtonListener(new SegowayServer());
    
    GyroSensor gyro = new GyroSensor(SensorPort.S1);
    NXTMotor left = new NXTMotor(MotorPort.B);
    NXTMotor right = new NXTMotor(MotorPort.C);
    
    robot = new SegowayPilot(left, right, gyro, SegowayPilot.WHEEL_SIZE_NXT2, 16.5);
        
    while(true) {
      System.out.println("Server waiting.");
      NXTConnection conn = Bluetooth.waitForConnection();
      DataInputStream in = conn.openDataInputStream();
      System.out.println("Connected.");
      int reply = 0;
      do {
        try {
          reply = in.readInt();
          System.out.println("Command: " + reply);
        } catch (IOException e) {
          e.printStackTrace();
        }
        
        performMove(reply);
        
      } while(reply != QUIT);
      
      try {
        in.close();
        Thread.sleep(100);
        conn.close();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private static void performMove(int cmd) {
    switch (cmd) {
          case FWD:  
            robot.forward();
            break;
          case BWD:  
            robot.backward();
            break;
          case LEFT:  
            robot.rotate(Double.POSITIVE_INFINITY);
            break;
          case RIGHT:  
            robot.rotate(Double.NEGATIVE_INFINITY);
            break;
          case STOP:
            robot.stop();
            break;
    }
  }
  
  public void buttonPressed(Button arg0) {}

  
  public void buttonReleased(Button arg0) {
    System.exit(0);
  }
}