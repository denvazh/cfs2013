import lejos.nxt.*;
import lejos.robotics.localization.OdometryPoseProvider;
import lejos.robotics.navigation.DifferentialPilot;

public class Odometry {

	public static void main(String[] args) {
		double diam = DifferentialPilot.WHEEL_SIZE_NXT1;
		double width = 16.45;
		
		DifferentialPilot robot = new DifferentialPilot(diam, width, Motor.B, Motor.C);
		OdometryPoseProvider pp = new OdometryPoseProvider(robot); 
		
		robot.rotate(90);
		robot.travel(100);
		robot.arc(30, 90);
		robot.travel(50);
		
		System.out.println("End: " + pp.getPose());
		Button.waitForAnyPress();
	}
}