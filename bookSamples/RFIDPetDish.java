import lejos.nxt.*;
import lejos.nxt.addon.RFIDSensor;

public class RFIDPetDish {

	public static int CLOSING = 0;
	public static int OPENING = 1;
	public static int T_STAY_OPEN = 30000; // Delay before closing (millis)
	
	public static void main(String[] args) {
		int state = CLOSING;
		int oldState = CLOSING;
		Motor.B.setSpeed(50);
		Motor.C.setSpeed(50);
		RFIDSensor rfid = new RFIDSensor(SensorPort.S1);
		System.out.println(rfid.getProductID());
		System.out.println(rfid.getVendorID());
		System.out.println(rfid.getVersion());
		System.out.println("Put tag in front\nof sensor and \npress enter");
		Button.ENTER.waitForPressAndRelease();
		long tagNumber = rfid.readTransponderAsLong(false);
		Sound.beepSequenceUp();
		long time = 0;
		while(!Button.ESCAPE.isDown()) {
			long transID = rfid.readTransponderAsLong(true);
			if(transID == tagNumber) {
				System.out.println(transID);
				time = System.currentTimeMillis();
			}
			long curTime = System.currentTimeMillis();
			if(curTime - time > T_STAY_OPEN) 
				state = CLOSING;
			else
				state = OPENING;
			
			if(state != oldState) {
				oldState = state;
				if(state == CLOSING) {
					Sound.beepSequence(); // Warning noise
					try {Thread.sleep(1500);} catch (InterruptedException e) {}
					Motor.B.rotateTo(0, true);
					Motor.C.rotateTo(0, true);
				} else if(state == OPENING){
					Sound.beepSequenceUp();
					Motor.B.rotateTo(-90, true);
					Motor.C.rotateTo(-90, true);
				}
			}
			
			try {Thread.sleep(100);} catch (InterruptedException e) {}
		}
	}
}
