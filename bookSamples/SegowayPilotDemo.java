import lejos.nxt.*;
import lejos.nxt.addon.GyroSensor;
import lejos.robotics.navigation.*;

public class SegowayPilotDemo {

	public static void main(String [] args) throws Exception {
		NXTMotor left = new NXTMotor(MotorPort.B);
		NXTMotor right = new NXTMotor(MotorPort.C);
		
		GyroSensor g = new GyroSensor(SensorPort.S1);
				
		SegowayPilot pilot = new SegowayPilot(left, right, g, SegowayPilot.WHEEL_SIZE_NXT2, 10.45F); 
		pilot.setTravelSpeed(80);
		
		// Draw three squares
		for(int i=0;i<12;i++) {
			pilot.travel(50);
			pilot.rotate(90);
			Thread.sleep(2000);
		}
	}	
}
