import lejos.nxt.*;
import lejos.robotics.RegulatedMotor;

public class LARA {
  
  double BASE_ARM_LENGTH = 16.3; // cm's
  double FOREARM_LENGTH = 19.3;
  double DISTANCE_FROM_CENTER = 6.5; // dist from center axis
  
  int ZAXIS_START_ANGLE = -90;
  int SHOULDER_START_ANGLE = 90;
  int ELBOW_START_ANGLE = 90;
    
    RegulatedMotor R;
    RegulatedMotor S;
    RegulatedMotor E;
  
  public static void main(String [] args) throws Exception{
    LARA a = new LARA();
    
    // Pick up part:
    a.gotoPoint(0, 25, -7);
    a.gotoPoint(0, 35, -5);
    a.gotoPoint(0, 50, 10);
    
    a.gotoPoint(-15, 10, -30); // drop part
    
    a.gotoPoint(-15, 10, 30); // return
    a.gotoPoint(0, 40, 15); 
  }
  
  public LARA() {
    S = calibrate(MotorPort.B, false, -115); // Shoulder
    E = calibrate(MotorPort.C, false, -81); // Elbow
    R = calibrate(MotorPort.A, true, 160); // Rotator

        R.setSpeed(400); // Rotator
    R.setAcceleration(200);
    S.setSpeed(400); //Shoulder
    S.setAcceleration(200);
    E.setSpeed(400); // Elbow
    E.setAcceleration(200);
  }
  
  public RegulatedMotor calibrate(MotorPort port, boolean reverse, int target) {
    NXTMotor motor = new NXTMotor(port);
    motor.setPower(20);
    if(reverse)
      motor.backward();
    else 
      motor.forward();
    int old = -999999;
    while(motor.getTachoCount() != old) {
      old = motor.getTachoCount();
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
      }
    }
    RegulatedMotor reg_motor = new NXTRegulatedMotor(port);
    reg_motor.setSpeed(150);
    reg_motor.resetTachoCount();
    reg_motor.rotate(target);
    reg_motor.resetTachoCount();
        return reg_motor;
  }
    
  public void gotoPoint(double x, double y, double z) {
    // 1. Calculate Z-Axis angle on shoulder
    double zaxisangle = Math.atan2(-y,x); // -y because motor is reverse?

    // 2. Calculate shoulder angle:
    double Z = Math.sqrt(y * y + x * x);
    Z = Z - DISTANCE_FROM_CENTER; // Corrected due to arm construction
    double c = Math.sqrt(Z * Z + z * z);
    double angle1 = Math.asin(z/c);
    double angle2 = Math.acos((BASE_ARM_LENGTH * BASE_ARM_LENGTH + c * c - FOREARM_LENGTH * FOREARM_LENGTH)/(2*BASE_ARM_LENGTH*c));
    double shoulderangle = angle1 + angle2;

    // 3. Calculate elbow angle:
    double elbowangle = Math.acos((Math.pow(BASE_ARM_LENGTH, 2) + Math.pow(FOREARM_LENGTH, 2) - Math.pow(c, 2))/(2*BASE_ARM_LENGTH*FOREARM_LENGTH));

    rotateZAxisTo(Math.toDegrees(zaxisangle));
    rotateShoulderTo(Math.toDegrees(shoulderangle));
    rotateElbowTo(Math.toDegrees(elbowangle));
    
    while(R.isMoving() || S.isMoving() || E.isMoving()) {
      Thread.yield();
    }
    
    Sound.beep();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {}
  }
  
  /**
   * Rotate elbow up or down.
   * @param angle +ve value is up.
   */
  public void rotateElbowTo(double angle) {
    angle = angle - ELBOW_START_ANGLE;
    E.rotateTo((int)-angle, true);
  }
  
  /**
   * Rotate shoulder up or down.
   * @param angle +ve value is up.
   */
  public void rotateShoulderTo(double angle) {
    double ratio = 36F/12; // Gear ratio 36:12
    angle = angle - SHOULDER_START_ANGLE;
    S.rotateTo((int)(angle * ratio), true);
  }
  
  /**
   * Rotate base of arm.
   * @param angle +ve value is counterclockwise
   */
  public void rotateZAxisTo(double angle) {
    double ratio = 20F/12; // Gear ratio 20:12
    angle = angle - ZAXIS_START_ANGLE;
    R.rotateTo((int)(angle * ratio), true);
  }
}
