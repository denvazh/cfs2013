import lejos.robotics.navigation.*;
import lejos.nxt.*;

public class Rambler {

   public static final int AREA_WIDTH = 200;
   public static final int AREA_LENGTH = 200;
      
    public static void main(String[] args) throws Exception {
       
        DifferentialPilot p = new DifferentialPilot(DifferentialPilot.WHEEL_SIZE_NXT2, 15.5, Motor.B, Motor.C);
        Navigator nav = new Navigator(p);
        
        // Repeatedly drive to random points:
        while(!Button.ESCAPE.isDown()) {
           System.out.println("Target: ");
           double x_targ = Math.random() * AREA_WIDTH;
           double y_targ = Math.random() * AREA_LENGTH;
           System.out.println("X: " + (int)x_targ);
           System.out.println("Y: " + (int)y_targ);
           System.out.println("Press ENTER key");
           Button.ENTER.waitForPressAndRelease();
           
           nav.addWaypoint(new Waypoint(x_targ, y_targ));
           nav.addWaypoint(new Waypoint(0, 0, 0));
        }
    }
}