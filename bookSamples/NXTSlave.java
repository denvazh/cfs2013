import lejos.nxt.*;
import lejos.robotics.mapping.*;
import lejos.robotics.mapping.NavigationModel.NavEvent;
import lejos.robotics.navigation.*;
import lejos.robotics.navigation.Move.MoveType;
import lejos.robotics.objectdetection.*;

/**
 * Note: This class now includes a listener interface and uses a feature detector.
 * @author Lawrie
 *
 */
public class NXTSlave implements NavEventListener {
	public static final float MAX_DISTANCE = 50f;
	public static final int DETECTOR_DELAY = 1000;
	
	private NXTNavigationModel model;
	
	public static void main(String[] args) throws Exception {
		(new NXTSlave()).run();
	}
	
	public void run() throws Exception {
    	model = new NXTNavigationModel();
    	model.setDebug(true);
    	model.setSendMoveStart(true);

    	Button.waitForAnyPress();
    	model.shutDown();
	}

	public void whenConnected() {
    	
    	final DifferentialPilot robot = new DifferentialPilot(5.6, 16.4,Motor.B, Motor.C);
    	final Navigator navigator = new Navigator(robot);
    	UltrasonicSensor sonic = new UltrasonicSensor(SensorPort.S1);
    	RangeFeatureDetector detector = new RangeFeatureDetector(sonic, MAX_DISTANCE, DETECTOR_DELAY);
		
    	// Adding the navigator, adds the pilot and pose provider as well
    	model.addNavigator(navigator);
    	
    	// Add the feature detector and start it. 
    	// Give it a pose provider, so that it records the pose when a feature was detected
    	model.addFeatureDetector(detector);
    	detector.enableDetection(true);
    	detector.setPoseProvider(navigator.getPoseProvider());
    	
    	// Stop if an obstacle is detected, unless doing a rotate
    	detector.addListener(new FeatureListener() {
			public void featureDetected(Feature feature, FeatureDetector detector) {
				if (robot.isMoving() && robot.getMovement().getMoveType() != MoveType.ROTATE) {
					robot.stop();
					if (navigator.isMoving()) navigator.stop();
				}					
			}		
    	});
	}

	public void eventReceived(NavEvent navEvent) {
		// Nothing
	}
}
