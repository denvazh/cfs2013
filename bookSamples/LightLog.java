import lejos.nxt.*;
import lejos.robotics.LightDetector;
import lejos.util.*;

public class LightLog {
  public static void main(String[] args) {
    Datalogger dl = new Datalogger();
    System.out.println("Press ENTER");
    Button.ENTER.waitForPressAndRelease();
    LightDetector ld = new ColorSensor(SensorPort.S3);
    // LightDetector ld = new LightSensor(SensorPort.S3);

    while(!Button.ENTER.isDown()) {
      float val = ld.getNormalizedLightValue();
      int t = (int)System.currentTimeMillis();
      dl.writeLog(t, val);
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    dl.transmit();
  }
}