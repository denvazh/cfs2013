import lejos.nxt.*;
import lejos.nxt.addon.*;
import java.awt.Rectangle;

public class NXTCamTest {

	final static int INTERVAL = 500; // milliseconds

	public static void main(String[] args) throws Exception {
			
		NXTCam camera = new NXTCam(SensorPort.S1);
		
		System.out.println(camera.getProductID());
		System.out.println(camera.getVendorID());
		System.out.println(camera.getVersion());
		
		int numObjects;
		int counter = 0;
				
		camera.sendCommand('A'); // sort objects by size
		camera.sendCommand('E'); // start tracking

		int OBJ_MIN = 6; // Minimum size
		
		while (!Button.ESCAPE.isDown()) {
			System.out.println("Objects: " + (numObjects = camera.getNumberOfObjects()) + " counter = " + counter++);
			
			if (numObjects >= 1 && numObjects <=8) {
				Rectangle r = camera.getRectangle(0);
				if (r.height > OBJ_MIN || r.width > OBJ_MIN) {
					System.out.print("X: " + r.x);
					System.out.println(" Y: " + r.y);
					System.out.print("Ht: " + r.height);
					System.out.println(" Wd: " + r.width);		
				}
			}
			Thread.sleep(INTERVAL);
		}
	}
}