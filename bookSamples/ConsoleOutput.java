import lejos.nxt.comm.RConsole;

public class ConsoleOutput {

	public static void main(String[] args) throws Exception {
		RConsole.openAny(0);
		
		for(int i=0;i<100;i++) {
			System.out.println("Count " + i);
			RConsole.println("Counting " + i);
		}
		Thread.sleep(500);
		RConsole.close();
	}
}
