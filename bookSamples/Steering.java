import lejos.nxt.*;
import lejos.robotics.navigation.*;

public class Steering {

   public static void main(String[] args) {
      // Make sure to use correct tire size and track-width!
      ArcMoveController pilot = new DifferentialPilot(5.6, 16.4, Motor.B, Motor.C);
      pilot.setMinRadius(34);
      Navigator nav = new Navigator(pilot);
      nav.goTo(40, 50, 90);
      Button.ENTER.waitForPressAndRelease();
      nav.goTo(0, 0, 0);
      Button.ENTER.waitForPressAndRelease();
      nav.goTo(40, 50, 180);
   }
}
