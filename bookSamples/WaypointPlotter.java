import lejos.nxt.*;
import javax.microedition.location.*;
import java.io.*;
import java.util.Properties;

public class WaypointPlotter implements LocationListener {

	public static void main(String[] args) {
		
		// Get an instance of the provider
		LocationProvider lp = null;
		try {
			System.out.println("Connecting..");
			lp = LocationProvider.getInstance(null);
		} catch (LocationException e) {
			System.err.println(e.getMessage());
			Button.waitForAnyPress();
			System.exit(0);
		}
		
		LocationListener locationListener = new WaypointPlotter();
		lp.setLocationListener(locationListener, -1, 6, 0);
		
		Properties p = new Properties();
		Button.setKeyClickTone(Button.ENTER.getId(), 0); // Disable default sound
		int count = 0;
		int press;
		do {
			press = Button.waitForAnyPress();
			if(press == Button.ID_ENTER) {
				Sound.beepSequenceUp();
				Location l;
				try {
					l = lp.getLocation(-1);
				} catch (Exception e) {
					System.err.println(e.getMessage());
					continue;
				}
				Coordinates c = l.getQualifiedCoordinates();
				p.setProperty("lat" + count, "" + c.getLatitude());
				p.setProperty("long" + count++, "" + c.getLongitude());
			}
		} while(press != Button.ID_ESCAPE);
		p.setProperty("waypoints", "" + count);
		File f = new File("waypoints.prop");
		if(!f.exists())
			try {
				f.createNewFile();
				FileOutputStream out = new FileOutputStream(f);
				p.store(out, "GPS Waypoints");
				out.close();
			} catch (IOException e) {
				System.err.println("Save failed");
				System.err.println(e.getMessage());
				Button.waitForAnyPress();
				System.exit(0);
			}
	}
	
	public void locationUpdated(LocationProvider lp, Location l) {
		QualifiedCoordinates c = l.getQualifiedCoordinates();
		LCD.clearDisplay();
		LCD.drawString("Lat " + c.getLatitude(), 0, 1);
		LCD.drawString("Long " + c.getLongitude(), 0, 2);
		LCD.drawString("Hor.Acc " + c.getHorizontalAccuracy(), 0, 3);
		LCD.refresh();
	}

	public void providerStateChanged(LocationProvider lp, int state) {}
}