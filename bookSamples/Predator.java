import lejos.nxt.*;
import lejos.nxt.addon.*;
import java.awt.Rectangle;

public class Predator {

	final static int INTERVAL = 200; // milliseconds
	final static int FIELD_OF_VIEW = 52; // degrees camera covers
	final static int PIXEL_WIDTH = 176; // camera width
	final static int CAMERA_CENTER = PIXEL_WIDTH/2; // camera width
	
	final static int SCAN_ZONES = 10; // zones to scan
	
	public static void aim(Rectangle r) {
		int centreX = r.x + (r.width/2);
		int x_diff = centreX - CAMERA_CENTER;
		float x_ratio = (float)x_diff / PIXEL_WIDTH;
		float rotateDeg = x_ratio * FIELD_OF_VIEW;
		Motor.B.rotate((int)-rotateDeg);
	}
	
	public static void rotateToZone(int zone) {
		Motor.B.rotateTo(-zone * (FIELD_OF_VIEW - 10));
	}
	
	public static void main(String [] args) throws Exception {
		Motor.B.setSpeed(50);
		NXTCam cam = new NXTCam(SensorPort.S1);
		int numObjects;
		
		cam.setTrackingMode(NXTCam.OBJECT_TRACKING);
		cam.sortBy(NXTCam.SIZE);
		cam.enableTracking(true);
		
		int zone = 0;
		
		while(zone < SCAN_ZONES) {
			rotateToZone(zone);
			numObjects = cam.getNumberOfObjects();
						
			if (numObjects >= 1 && numObjects <= 8) {
				Sound.beep();
				Rectangle r = cam.getRectangle(0);
				Predator.aim(r);
				Motor.A.rotate(360);
			}
			
			++zone;
			Thread.sleep(INTERVAL);
		}
	}
}