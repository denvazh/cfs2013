import lejos.nxt.*;
import lejos.robotics.*;
import lejos.robotics.localization.MCLPoseProvider;
import lejos.robotics.mapping.NXTNavigationModel;
import lejos.robotics.navigation.*;

public class NXTMCL {
	private static final int GEAR_RATIO = -12;
	private static final boolean ROTATING_RANGE_SCANNER = false;
	private static final RegulatedMotor HEAD_MOTOR = Motor.A;
	private static final float[] ANGLES = {-45f,0f,45f};
	private static final int BORDER = 0;
	private static final double ROTATE_SPEED = 100f;
	private static final double TRAVEL_SPEED = 100f;
	private static final float MAX_DISTANCE = 40f;
	private static final float CLEARANCE = 20f;
	
	public static void main(String[] args) throws Exception {
    	
    	DifferentialPilot robot = new DifferentialPilot(5.6, 16.4, Motor.B, Motor.C);
    	robot.setRotateSpeed(ROTATE_SPEED);
    	robot.setTravelSpeed(TRAVEL_SPEED);
    	RangeFinder sonic = new UltrasonicSensor(SensorPort.S1);
    	RangeScanner scanner;
    	if (ROTATING_RANGE_SCANNER)scanner = new RotatingRangeScanner(HEAD_MOTOR, sonic, GEAR_RATIO);
    	else scanner = new FixedRangeScanner(robot, sonic);
    	scanner.setAngles(ANGLES);
    	// Map and particles will be sent from the PC
    	MCLPoseProvider mcl = new MCLPoseProvider(robot, scanner, null, 0, BORDER);
    	Navigator navigator = new Navigator(robot, mcl); 	
    	NXTNavigationModel model = new NXTNavigationModel();
    	model.setDebug(true);
    	model.setRandomMoveParameters(MAX_DISTANCE, CLEARANCE);
    	// Adding the Navigator also adds the pilot, pose provider and scanner
    	model.addNavigator(navigator);
    	// Don't send the pose automatically - PC requests it when required
    	model.setAutoSendPose(false);
	}
}
