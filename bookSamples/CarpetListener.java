import lejos.nxt.*;
import lejos.robotics.navigation.*;

public class CarpetListener implements MoveListener {
   
   public static void main(String [] args) {
      MoveListener listener = new CarpetListener();
      DifferentialPilot robot = new DifferentialPilot(4.32, 16.5, Motor.B, Motor.C, false);
      robot.addMoveListener(listener);
      UltrasonicSensor us = new UltrasonicSensor(SensorPort.S1);
      robot.forward();
      while(!Button.ESCAPE.isDown()) {
         if(us.getDistance() < 40) {
            robot.travel(-20);
            robot.rotate(45);
            robot.forward();
         }
      }
   }

   public void moveStarted(Move move, MoveProvider mp) {}

   public void moveStopped(Move move, MoveProvider mp) {
      System.out.println("Moved " + (int)move.getDistanceTraveled() + " cm");
   }
}
