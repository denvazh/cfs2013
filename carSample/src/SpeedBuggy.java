import lejos.nxt.*;
import java.awt.event.*;
import java.awt.*;

public class SpeedBuggy extends Frame implements KeyListener{
	final int FORWARD = 87, // W = forward
	BACKWARD = 83, // S = backward
	LEFT = 65, // A = left turn
	RIGHT = 68, // D = right
	QUIT = 81; // Q = quit

	static final int LEFT_TACHO = -80,
	RIGHT_TACHO = 80,
	CENTER_TACHO = 0;

	boolean steerreleased = true;
	boolean driverreleased = true;

	UltrasonicSensor us;

	public SpeedBuggy(String title){
		super(title);
		
		Motor.B.setSpeed(400); //Steering
		Motor.C.setSpeed(900); //Drive motor

		us = new UltrasonicSensor(SensorPort.S1);

		this.setBounds(0,0,300,50);
		this.addKeyListener(this);
		this.setVisible(true);
	}

	public void keyPressed(KeyEvent e){
		switch(e.getKeyCode()) {
			case FORWARD:
				if(driverreleased==true){
					Motor.C.backward();
					driverreleased = false;
				}
				break;
			case BACKWARD:
				if(driverreleased==true){
					Motor.C.forward();
					driverreleased = false;
				}
				break;
			case LEFT:
				if(steerreleased==true){
					Motor.B.rotateTo(LEFT_TACHO);
					steerreleased = false;
				}
				break;
			case RIGHT:
				if(steerreleased==true){
					Motor.B.rotateTo(RIGHT_TACHO);
					steerreleased = false;
				}
				break;
		}
	}

	public void keyReleased(KeyEvent e) {
		switch(e.getKeyCode()) {
			case FORWARD:
			case BACKWARD:
				Motor.C.flt();
				driverreleased = true;
				break;
			case LEFT:
			case RIGHT:
				Motor.B.rotateTo(CENTER_TACHO);
				steerreleased = true;
				break;
			case QUIT:
				Sound.beepSequenceUp();
				Sound.pause(1000);
				System.exit(0);
		}
		System.out.println("DIST: " + us.getDistance());
	}

	public void keyTyped(KeyEvent e) {}

	public static void main(String[] args){
		new SpeedBuggy("Enter commands");
	}
}

