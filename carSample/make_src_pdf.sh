#!/usr/bin/env bash

OUTPUT_FILENAME='carbuggy.pdf'
SPECIFIC_HDR="Remote Controlled Car Example"
GENERIC_HDR="Creative Factory Seminar 2013"

enscript -E -r --color -b "${SPECIFIC_HDR}||${GENERIC_HDR}" --file-align=2 --highlight --line-numbers -o - `find . -name '*.java'` | ps2pdf - $OUTPUT_FILENAME
