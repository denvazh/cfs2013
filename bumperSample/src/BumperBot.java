import lejos.nxt.*;
import lejos.robotics.Touch;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.objectdetection.*;

/**
 * This bumper-car example uses a FusorDetector to detect and report objects from multiple sensors.
 * Use any pilot robot with an ultrasonic sensor plugged into port 4, and a touch sensor plugged into port 2.
 * The touch sensor is the bumper for detecting objects the ultrasonic fails to detect.
 */
public class BumperBot implements FeatureListener {

	private static final int MAX_DETECT = 50;
	private static final int RANGE_READING_DELAY = 500;
	private static final int TOUCH_X_OFFSET = 0;
	private static final int TOUCH_Y_OFFSET = -14;

	private DifferentialPilot robot;

	public BumperBot() {
		robot = new DifferentialPilot(4.32, 16.35, Motor.B, Motor.C, false);
		robot.forward();
	}

	public static void main(String[] args ) throws Exception {
		UltrasonicSensor us = new UltrasonicSensor(SensorPort.S4);
		FeatureDetector usdetector = new RangeFeatureDetector(us, MAX_DETECT,RANGE_READING_DELAY, 90);
		
		Touch ts = new TouchSensor(SensorPort.S1);
		FeatureDetector tsdetector = new TouchFeatureDetector(ts, TOUCH_X_OFFSET, TOUCH_Y_OFFSET); 

		FusorDetector fusion = new FusorDetector();
		fusion.addDetector(tsdetector);
		fusion.addDetector(usdetector);
		fusion.addListener(new BumperBot());
		Button.waitForAnyPress();
	}

	public void featureDetected(Feature feature, FeatureDetector detector) {
		System.out.println("R:" + feature.getRangeReading().getAngle());
		if(feature.getRangeReading().getAngle() >= 0) {
			detector.enableDetection(false);
			robot.rotate(90 * Math.random());
			detector.enableDetection(true);
			robot.backward();	
		} else {
			robot.forward();
		}
	}
}
