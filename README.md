# Creative Factory Seminar 2013

Sample programms and blueprints for the lectures.

Examples included are:

- Touch Sensor
- Ultrasonic Sensor
- Color Sensor
- Motor Carpet Rover
- Motor Carpet Rover with Bumper
- CarBuggy Sample
